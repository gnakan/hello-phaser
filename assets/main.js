var game = new Phaser.Game(500, 500, Phaser.AUTO, 'game-area', {preload:preload, create: create, update:update});

function preload(){
	game.load.image('sky', 'Blocks/skyBlock.png');
	game.load.image('grass', 'Blocks/grassDirtBlock.png');
};

function create(){
	game.add.tileSprite(0,0,500,500, 'sky'); //tile the background image

	game.add.tileSprite(0,468,500,32, 'grass'); //create the ground
};

function update(){

};